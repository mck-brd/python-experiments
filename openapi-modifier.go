package main

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
)

func main() {
	file := getFile()
	fmt.Println(file.Name())
	contents, fOpenError := ioutil.ReadFile(file.Name())

	if fOpenError != nil {
		os.Exit(1)
	}

	// unstructuredJson := `{"os": {"Windows": "Windows OS","Mac": "OSX","Linux": "Ubuntu"},"compilers": "gcc"}`

	unstructuredJSON := contents

	var result map[string]interface{}

	jsonError := json.Unmarshal([]byte(unstructuredJSON), &result)
	if jsonError != nil {
		os.Exit(2)
	}

	fmt.Println(result)
	walk(result)
	fmt.Println(result)
	newJSON, jsonError := json.MarshalIndent(result, "", "  ")
	if jsonError != nil {
		fmt.Println("Could not marshal json")
	}

	writeError := ioutil.WriteFile("openapi.json", newJSON, 0644)

	if writeError != nil {
		fmt.Println("Error writing json file")
	}
}

func getFile() fs.FileInfo {
	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if strings.HasPrefix(file.Name(), "openapi") && strings.HasSuffix(file.Name(), "json") {
			// fmt.Println(file.Name(), file.IsDir())
			return file
		}
	}

	return nil
}

func walk(node interface{}) {

	myMap, ok := node.(interface{})
	if !ok {
		return
	}
	// fmt.Println(myMap)

	if reflect.ValueOf(node).Kind() == reflect.Map {

		for x, v := range myMap.(map[string]interface{}) {
			//	fmt.Println("x:")
			//	fmt.Println(x)
			//	fmt.Println("v:")

			// fmt.Println(v)

			if x == "security" || x == "securitySchemes" {
				delete(myMap.(map[string]interface{}), x)
			}
			walk(v)
		}
	} else {
		// fmt.Println(node)
	}
}
